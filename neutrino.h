#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <array>
#include <string>


#include "DataHelper.hh"

#include "TROOT.h"
#include "TBrowser.h"
#include "Riostream.h"
#include "TH1.h"
#include "TFile.h"
#include "TRandom.h"
#include "TString.h"
#include "TTree.h"
#include "TBranch.h"
#include "TGraphErrors.h"
#include "TList.h"
#include "TObjArray.h"
#include "TChain.h"
#include "TSystem.h"
#include "TMath.h"
#include "TVector3.h"
#include "TCanvas.h"
#include "TRegexp.h"
#include "TMultiGraph.h"
#include "TLegend.h"

bool create_hist(std::vector<TH1D>& h);

bool create_hist_exp(std::vector<TH1D> &h, std::vector<TGraphErrors> &exp, TString format);

bool create_tgs(std::vector<TGraph> &tg, std::vector<TGraph> &tgl);

bool analisis(TString tname, std::vector<double>& ev,  vector<double> &evl);

bool setgr(int idx, TString E, std::vector<double>& vals,  vector<TGraph> &grs);

void set_h(double ang, std::vector<TH1D>& h, double k);

Double_t theta_calc(Double_t px, Double_t py, Double_t pz);

void save_hist(std::vector<TH1D>& h);

void load_graphs();

void find_data(TString name, TString &nuc, TString &en);

void plotdata(TGraph *ptg, int pos);
void plotdata2(TGraph *ptg, TGraph *ptgl,int pos);


