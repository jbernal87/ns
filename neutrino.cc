#include "neutrino.h"

using namespace std;

int main() {

	gSystem->Load("libTree.so");
	fstream file;
	file.open ("files.txt", ios::in);
	
	cout << "begin run " << endl;
	
	if(file.is_open())
		cout << "Abrio bien: " << "file.txt"  << endl;
	if(file.fail()) 
		cout << "problemas con el archivo" << endl;

	vector<TH1D> hist; 
	vector<TGraphErrors> exp;  

	vector<TGraph> tg;  
	vector<TGraph> tgl;  

	hist.reserve(10);
	TH1D untest("ENeutronall", " ;Energy (MeV); Probability", 600, 0, 1200);
	cout << hist.size() << endl;
	hist.push_back(untest);  		

	vector<TString> fnames;
	fnames.reserve(20);

	vector<double> events;
	events.reserve(8);

	vector<double> events_like;
	events_like.reserve(8);

		
		while(true) {      
		if (!file.good() ) break; 
		string pdline, Stemp;
		getline(file, pdline);   
		istringstream istr(pdline.c_str());      
		istr >> Stemp;
		if(Stemp == "" )
		break;
		fnames.push_back(Stemp);
				
		} 
	
	file.close();
	cout << fnames.size() << endl;
	
	TString nuc;
	TString nuctem;
	TString en;
	find_data(fnames[0],nuc, en);
	cout << "test" << endl;
	cout << "nuc " << nuc << endl;
	cout << "energy " << en << endl;	


	//create_hist_exp(hist,exp,find_data(fnames[0]));

	create_tgs(tg,tgl);


			
        int mycounter = 0;
	for (auto it = fnames.begin() ; it != fnames.end(); ++it){
		analisis(*it, events, events_like);
		cout << endl;
		mycounter++;
      		find_data(*it,nuc, en);
		cout << nuc << " " << en << endl; 
		setgr(mycounter,en,events,tg);
		setgr(mycounter,en,events_like,tgl);   
		for(int i=0; i<8; i++ ){
//			cout << events[i] << " ";	
			events[i]=0;
			events_like[i] = 0;	
		}	
	}

	

/*	for(int i=0; i<8; i++ ){
		cout << events[i] << " ";	
		events[i]=0;
		events_like[i] = 0;	
		}
	cout << endl;
*/
//	save_hist(hist);

	TString name = "ccq.pdf";

	array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};	 
	cout << reaction[2] << endl;
	
/*	for(int i=0; i<8; i++ ){	
		TCanvas *c1 = new TCanvas("c1","",800,600) ;
		tg[i].Draw("AP");
//	c1->SetLogy();
		c1->Print(reaction[i]+name);
	delete c1;
	}*/
/*
	for(int i=0; i<8; i++ ){	
		TCanvas *c1 = new TCanvas("c1","",800,600) ;
		tgl[i].Draw("AP");
//	c1->SetLogy();
		c1->Print(reaction[i]+"like"+name);
	delete c1;
	}*/
	
	
	for(int i=0; i<8; i++ ){	
	  plotdata2(&tg[i],&tgl[i],i);
	  
	}
	
	return 0;
}



bool create_tgs(vector<TGraph> &tg, vector<TGraph> &tgl){

        array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};
	TString hnameFormat = "_like" ;
	
	TGraph temp;
	TGraph temp1;
	int n = 15;	

  	for ( auto it = reaction.begin(); it != reaction.end(); ++it ){
		//		cout << hname << " " << endl;
		temp1 = TGraph(30);
		temp1.SetTitle(*it);
		temp1.SetMarkerStyle(21);
		temp1.SetMarkerSize(0.8);
/*		temp1.GetXaxis()->SetTitle("Energy");
		temp1.GetXaxis()->CenterTitle();
		temp1.GetYaxis()->SetTitle("Probability");
		temp1.GetYaxis()->CenterTitle();
*/		tg.push_back(temp1);
		temp1.SetTitle(*it+hnameFormat);				
		tgl.push_back(temp1);	
		}



 return 0;
}

bool create_hist(vector<TH1D> &h){
	/*

	TString r1 = "";
	TString r2 = "";
	TString r3 = "";
	TString r4 = "";
	TString r5 = "";
	TString r6 = "";
	TString r7 = "";


	 array< TString,13 > reaction ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};
	 array< TString,8 > reaction ={{"mp","mppp","mppn","mpop","nuppn","nupop","nupon","nupmp"}};


	
	  TString hnameFormat = " " ;
       	  TString hname;

	  TString hnameFormat1 = "_like" ;
       	  TString hname1; 	


 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;


	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}*/
	return true;
}


bool create_hist_exp(vector<TH1D> &h, vector<TGraphErrors> &exp, TString format){
	
	/* array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};

	  array< TString,13 > angle ={ {"10","25","30","40","60","85","100","115","120","130","145","150","160"}};		

	  TString hnameFormat = "Ejected Neutrons at " ;
       	  TString hname; 	
	  TH1D htemp;	
	  int min = 0, max = 1200, mbins = 600;

	
	TString nametest;		        
	 cout << " test   " << angle.at(0) << endl;;

	  for ( auto it = angle.begin(); it != angle.end(); ++it ){
		hname = hnameFormat + *it;	
		nametest =  format+"ddxs_"+*it+"_leray.txt";
		cout << nametest  << endl;		
//		cout << hname << " " << endl;
		htemp = TH1D(hname, " ;Energy (MeV); Probability", mbins, min, max);
		h.push_back(htemp);		
		}*/
	return true;
}

bool analisis(TString tname, vector<double> &ev,  vector<double> &evl){
	
	TFile* f = NULL;

	cout << tname << endl;
		
	TBranch *TBPart_NPart, *TBPart_Mes_PID, *TBPart_E, *TBPart_PX, *TBPart_PY, *TBPart_PZ, *TBPart_K;	
	TBranch *TBNuc_A, *TBNuc_Z, *TBNuc_Energy, *TBNuc_phoCnt, *TBPart_PID, *TBinitialAttempts;
	
	
	
	f = new TFile(tname);
	


		
	
	if (f->IsZombie()) {
		cout << "Error opening file " << tname << endl;
		delete f;
		return false;
	} 
	else{
		cout << tname << " file sucessfully openend" << endl;
	//	f->ls();
		
		TTree *t2 = (TTree*) f->Get("History");
		
		Int_t A; Int_t Z; 
		Int_t initialAttempts=0;
		/* other stuff */
		Double_t cascatas_efetivas = 0;                             /// effective cascades counter
		Double_t neventos = 0;                                      /// number of attempts counter for the effective cascades
		Double_t An = 12;     
		double Zn = 6.;
		Double_t rnt = 1.18 * pow(double(An),1./3.);                /// Nuclear radius 
		Double_t sf = TMath::Pi() * pow((rnt),2.)*10.;                  /// fm^2 -> microbarn 


		Double_t nucEnergy; 
		Int_t phoCnt; Int_t NPart; 
		Int_t Part_PID[200]; 
		Double_t Part_PX[200];
		Double_t Part_PY[200]; 
		Double_t Part_PZ[200]; 
		Double_t Part_E[200];
		Double_t Part_K[200]; 
		
		TBNuc_A = t2->GetBranch("finalNuc_A");
		TBNuc_A->SetAddress(&A);
		TBNuc_Z = t2->GetBranch("finalNuc_Z");
		TBNuc_Z->SetAddress(&Z);
		TBNuc_Energy = t2->GetBranch("finalNuc_Ex");
		TBNuc_Energy->SetAddress(&nucEnergy);
		TBPart_NPart = t2->GetBranch("UnBindPart_Number");
		TBPart_NPart->SetAddress(&NPart);
		TBPart_PID = t2->GetBranch("UnBindPart_Pid");
		TBPart_PID->SetAddress(&Part_PID[0]);
		TBPart_PX = t2->GetBranch("UnBindPart_PX");
		TBPart_PX->SetAddress(&Part_PX[0]);
		TBPart_PY = t2->GetBranch("UnBindPart_PY");
		TBPart_PY->SetAddress(&Part_PY[0]);
		TBPart_PZ = t2->GetBranch("UnBindPart_PZ");
		TBPart_PZ->SetAddress(&Part_PZ[0]);
		TBPart_K = t2->GetBranch("UnBindPart_K");
		TBPart_K->SetAddress(&Part_K[0]);
		TBPart_E = t2->GetBranch("UnBindPart_E");
		TBPart_E->SetAddress(&Part_E[0]);
		t2->SetBranchAddress("initialAttempts",&initialAttempts);
		

				int dmcounter=0;

		std::vector<int> conteos;
		conteos.reserve(8);

		for (int k=0; k<8; k++){
			conteos[k]=0;
			ev[k]=0.;
			evl[k]=0.;	
		}
	//	cout << conteos.size() << endl;
		//int cn = 0,cp = 0,cm=0 ,cne = 0,cpm = 0,cp0 = 0,cpp = 0;			

		Int_t nentries = (Int_t)t2->GetEntries();
		cout << nentries << endl;

		int false_counter = 0;		
		for (Int_t i=0; i<nentries; i++) { // 
		t2->GetEntry(i);
			
			if(nucEnergy<0.0)
				{
					false_counter = false_counter +1;
					continue;	
				}
			else{
			  				
				 neventos += initialAttempts;    // Number of attempts to get an effective cascade
				 cascatas_efetivas++; // Count of the attempts and effective cascades
				
				for (int k=0; k<7; k++){
				conteos.push_back(0);
				}

				for (Int_t j=0; j < NPart; j++) {
				// aqui va el analisis de las partículas
		
					if(Part_PID[j]==2112)
						conteos[0]+=1; //neutrones

					if(Part_PID[j]==2212)
						conteos[1]+=1; //protones

					if(Part_PID[j]==13)
						conteos[2]+=1; //muones

					if(Part_PID[j]==211)
						conteos[3]+=1; // pi mas


					if(Part_PID[j]==111)
						conteos[4]+=1; // pi 0

					if(Part_PID[j]==-211)
						conteos[5]+=1; // pi menos

					if(Part_PID[j]==14)
						conteos[6]+=1; //neutrino
					

					if(Part_PID[j]==2214)
						dmcounter++;
					}
					
							
					if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==0& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[0]+=1.; //mp 

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==1& conteos[3]==0& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[0]+=1.; //mp like

					if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[1]+=1.; // mppp

					if(conteos[0]==0 & conteos[1]>=0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[1]+=1.; // mppp like 

                                        if(conteos[0]==1 & conteos[1]==0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						ev[2]+=1.; // mppn

					if(conteos[0]>=0 & conteos[1]==0& conteos[2]==1& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==0)
						evl[2]+=1.; // mppn like 

                                        if(conteos[0]==0 & conteos[1]==1& conteos[2]==1& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==0)
						ev[3]+=1.; // mpop

					if(conteos[0]>=0 & conteos[1]>=0 & conteos[2]==1& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==0)
						evl[3]+=1.; // mpop like 
                                     
                                        if(conteos[0]==1 & conteos[1]==0& conteos[2]==0& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==1)
						ev[4]+=1.; // nuppn

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==0& conteos[3]==1& conteos[4]==0& conteos[5]==0& conteos[6]==1)
						evl[4]+=1.; // nuppn like 

                                        if(conteos[0]==0 & conteos[1]==1& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						ev[5]+=1.; // nupop

					if(conteos[0]==0 & conteos[1]>=0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						evl[5]+=1.; // nupop like 
					
					if(conteos[0]==1 & conteos[1]==0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						ev[6]+=1.; // nupon

					if(conteos[0]>=0 & conteos[1]==0& conteos[2]==0& conteos[3]==0& conteos[4]==1& conteos[5]==0& conteos[6]==1)
						evl[6]+=1.; // nupon like 

                                        if(conteos[0]==0 & conteos[1]==1& conteos[2]==0& conteos[3]==0& conteos[4]==0& conteos[5]==1& conteos[6]==1)
						ev[7]+=1.; // nupmp

					if(conteos[0]>=0 & conteos[1]>=0& conteos[2]==0& conteos[3]==0& conteos[4]==0& conteos[5]==1& conteos[6]==1)
						evl[7]+=1.; // nupmp like 



/*
					if(conteos[0] & conteos[1]& conteos[2]& conteos[3]& conteos[4]& conteos[5]& conteos[6])	

					if(conteos[2]!=0 & (conteos[3]!=0 & (conteos[0]!=0 & conteos[1]==0)))
						ev[2]=ev[2]+1.;	// mppp
					if(conteos[2]!=0 & conteos[3]!=0 & conteos[0]!=0 & conteos[1]!=0 )
						ev[3]=ev[3]+1.;	// 				
					if(conteos[2]==0 & conteos[3]!=0 & conteos[0]!=0 & conteos[1]!=0 )
						ev[4]=ev[4]+1.;					
*/

					for (int k=0; k<7; k++){
					conteos[k]=0;
	//				cout << conteos[k] << endl;
					}


	//			if(i<10)
	//	cout << cn << " " << cp << " " << cm << " " << cp0 << " " << cpp << " " << cpm << " " << cne <<  endl;

				}	
	
			}
		
		
	//	double peso[8] = {0.02,0.025,0.025,0.04,0.03,0.037,0.002,0.004};
		
	//	double peso[8] = {1,1,1,1,1,1,1,1};
	        double peso[8] = {0.06,0.03,0.05,0.07,0.02,0.03,0.04,0.02};
	
		double nucleon[8] = {0};
		nucleon[0] = An-Zn;
		for (int l = 1; l<8; l++){
		  nucleon[l] = An;
		}
		
	//	double factor =  sf / (double(nentries - false_counter));
        double factor =  sf / ((double(neventos))*10);
        
		cout << " factor " << factor << endl;
		
		for (int k = 0; k<8; k++){
		//cross section total	
			cout << ev[k];
			ev[k] =(ev[k]*factor)/(peso[k]*nucleon[k]);
			cout << " "<< ev[k] << endl ;
		//	cout << ev[k] << endl;
			evl[k] =(evl[k]*factor)/(peso[k]*nucleon[k]);
			
		//multiplicity	
			
	}

		delete t2;
		delete f;
	

	
	return true;
	}
	
	
	
}




Double_t theta_calc(Double_t px, Double_t py, Double_t pz){
/*
	TVector3 vec_x (1.,0.,0.);
 	TVector3 vec_P(px,py,pz);
 	
 	Double_t theta = vec_P.Angle(vec_x)*(180.)/(TMath::Pi());
 	
	return theta;*/
	return 1.;

} // Cierro P_calc



void set_h(double ang, std::vector<TH1D>& h, double k){ /*
	h[0].Fill(k);
	
	if( ang>7 && ang<12 )
		h[1].Fill(k);	
	else if ( ang>22 && ang<26 )
		h[2].Fill(k);
	else if ( ang>27 && ang<32 )
		h[3].Fill(k);
	else if ( ang>37 && ang<42 )
		h[4].Fill(k);
	else if ( ang>57 && ang<62 )
		h[5].Fill(k);
	else if ( ang>82 && ang<87 )
		h[6].Fill(k);
	else if ( ang>97 && ang<102 )
		h[7].Fill(k);
	else if ( ang>112 && ang<117 )
		h[8].Fill(k);
	else if ( ang>117 && ang<122 )
		h[9].Fill(k);
	else if ( ang>127 && ang<132 )
		h[10].Fill(k);
	else if ( ang>142 && ang<146 )
		h[11].Fill(k);
	else if ( ang>147 && ang<152 )
		h[12].Fill(k);
	else if (ang>157 && ang<162)
		h[13].Fill(k);*/

/*
	switch ((int)(ang)){
		case 7 ... 12:
			h[1].Fill(k);		
		case 22 ... 26:
			h[2].Fill(k);
		case 27 ... 32:
			h[3].Fill(k);
		case 37 ... 42:
			h[4].Fill(k);
		case 57 ... 62:
			h[5].Fill(k);
		case 82 ... 87:
			h[6].Fill(k);
		case 97 ... 102:
			h[7].Fill(k);
		case 112 ... 116:
			h[8].Fill(k);
		case 117 ... 122:
			h[9].Fill(k);
		case 127 ... 132:
			h[10].Fill(k);
		case 142 ... 146:
			h[11].Fill(k);
		case 147 ... 152:
			h[12].Fill(k);
		case 157 ... 162:
			h[13].Fill(k);

	

	}*/

}

void save_hist(std::vector<TH1D>& h){
	/*
	TFile* myfile;
	myfile = new TFile("cascade.root","recreate");

	for(int i=0; i<h.size(); i++){
		h[i].Write();	
		
	}
	myfile->Close();
	delete myfile;*/

}


void find_data(TString name, TString &nuc, TString &en){
	   int n1 = name.Last('/')+1;
	   int n2 = name.Last('M');
	   int n3 = name.First('_');
  	  nuc = TString(name(n1,n3-n1));	
	  en  = TString(name(n3+1,n2-n3-1));			
	
}


bool setgr(int idx, TString E, std::vector<double>& vals,  vector<TGraph> &grs){

	for (int i=0; i<8; i++ ) {	
		grs[i].SetPoint(idx,E.Atof()/1000.,vals[i]);
//		cout << vals[i] << endl;
	}

	return true;

}







void plotdata(TGraph *ptg, int pos){
    
  TMultiGraph* mg[8]; // 
  
    for(int i = 0; i < 8; i++){
      mg[i] = new  TMultiGraph();
    }
    
        mg[pos]->Add(ptg);
    
  const int dzise = 18; 
  TGraphErrors edata[dzise];
 
  int counter = 0;
  int counter2 = 0;
  
  TString basecc  = "CCqe%de";
  TString ccname;
  
  bool deu = true; 
  bool carb = false;  
  bool Al =  false;  
  bool Nomad =  false; 
  bool cern =  false; 

  
  //  cout << ccname << endl;
  
  for(int i = 1; i < 6; i++) {
    ccname= Form(basecc.Data(), i);
//    cout << ccname << endl;
    edata[counter] = TGraphErrors("data/"+ccname+".txt","%lg %lg %lg");
    mg[counter2]->SetTitle("#nu + n #rightarrow #mu^{-} + p ");
    
    if(deu && i==1)
      mg[counter2]->Add(&edata[counter]);

    if(carb && i>1 && i<4 )
      mg[counter2]->Add(&edata[counter]);
    
    if(Al && i==4)
      mg[counter2]->Add(&edata[counter]);

    if(Nomad && i==5)
      mg[counter2]->Add(&edata[counter]);
    
        counter++;
  }
  
  counter2++;
  
  TString baseres  = "CCres%de";
  TString resname;

  
  for(int i = 1; i < 8; i++) {
    resname= Form(baseres.Data(), i);
 //   cout << resname << endl;
    edata[counter] = TGraphErrors("data/"+resname+".txt","%lg %lg %lg");
        
  
      if(i<3){
	    mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{++} ");
	if(deu && i==2){
	  counter++;
	  continue;
	}
//      mg[counter2]->Add(&edata[counter]);
      } // end res1 
      
	  
      if(i==4){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi{+} + n "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==5 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
      }
      
        if(i==6){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi{0} + p "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==7 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
  
      }
      
    cout << counter2 << " c2 " << endl;  
    mg[counter2]->Add(&edata[counter]);	  
    counter++;  
  }

  
  counter2++;
  
  TString Nbaseres  = "NCres%de";
  TString Nresname;

  TString name[4] = {"#nu+p #rightarrow #nu + #Delta^{+} decay in #pi^{+} + n ","#nu+p #rightarrow #nu + #Delta^{+} decay in #pi{0} + p ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{0} + n ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{-} + p "};
  
     
  Nresname= Form(Nbaseres.Data(), 1);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[0]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;

  
  Nresname= Form(Nbaseres.Data(), 2);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 3);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;

  Nresname= Form(Nbaseres.Data(), 4);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[2]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  
  Nresname= Form(Nbaseres.Data(), 5);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 6);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
 // counter2++;
  
  
  cout << counter << endl;
  cout << counter2 << endl;

 // /home/jlbernal/cross/data/NCres1e.txt

  
      TString pbase  = "res/test%de.pdf";
      TString npr;
      for(int i = 0; i < 8; i++){
        npr= Form(pbase.Data(), i);
	cout << npr << endl;
       	TCanvas *c1 = new TCanvas("c1","",800,600) ;
	mg[i]->Draw("AP");
	c1->Print(npr);
	delete c1;
    } 
  
 
    TString pbase1  = "res/res%d.pdf";  
    TString outname = Form(pbase1.Data(), pos);;

    TCanvas *c1 = new TCanvas("c1","",800,600) ;
    mg[pos]->Draw("AP");
    mg[pos]->GetXaxis()->SetTitle("Energy (GeV)");
    mg[pos]->GetXaxis()->CenterTitle();
    mg[pos]->GetYaxis()->SetTitle("Cross Section (10^{-38} cm^{-2})");
    mg[pos]->GetYaxis()->CenterTitle();
    mg[pos]->GetYaxis()->SetTitleOffset(1.4); 
    mg[pos]->GetXaxis()->SetRangeUser(0.,1.6);

   gPad->Modified();
    //c1->SetLogx();  
    c1->Print(outname);
    delete c1;

 
    /*
     for(int i = 0; i < 8; i++){
      delete mg[i]; 
    }
   */
  
  
  cout << "test" << endl;
  
  
}


void plotdata2(TGraph *ptg, TGraph *ptgl,int pos){
    
  TMultiGraph* mg[8]; // 
  //TString tnames[8];
  
    for(int i = 0; i < 8; i++){
      mg[i] = new  TMultiGraph();
    }
    
	ptg->SetMarkerStyle(24);
	ptg->SetTitle("True");
	ptgl->SetMarkerStyle(23);
	ptgl->SetTitle("Like");
        mg[pos]->Add(ptg);
	mg[pos]->Add(ptgl);

    
  const int dzise = 18; 
  TGraphErrors edata[dzise];
 
  int counter = 0;
  int counter2 = 0;
  
  TString basecc  = "CCqe%de";
  TString ccname;
  
  bool deu = true; 
  bool carb = false;  
  bool Al =  false;  
  bool Nomad =  false; 
  bool cern =  false; 

  
  //  cout << ccname << endl;
  
  for(int i = 1; i < 6; i++) {
    ccname= Form(basecc.Data(), i);
//    cout << ccname << endl;
    edata[counter] = TGraphErrors("data/"+ccname+".txt","%lg %lg %lg");
    edata[counter].SetMarkerStyle(20);
    mg[counter2]->SetTitle("#nu + n #rightarrow #mu^{-} + p ");
    
    if(deu && i==1)
      mg[counter2]->Add(&edata[counter]);

    if(carb && i>1 && i<4 )
      mg[counter2]->Add(&edata[counter]);
    
    if(Al && i==4)
      mg[counter2]->Add(&edata[counter]);

    if(Nomad && i==5)
      mg[counter2]->Add(&edata[counter]);
    
        counter++;
  }
  
  counter2++;
  
  TString baseres  = "CCres%de";
  TString resname;

  
  for(int i = 1; i < 8; i++) {
    resname= Form(baseres.Data(), i);
 //   cout << resname << endl;
    edata[counter] = TGraphErrors("data/"+resname+".txt","%lg %lg %lg");
    edata[counter].SetMarkerStyle(20);    
  
      if(i<3){
	    mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{++} ");
	if(deu && i==2){
	  counter++;
	  continue;
	}
//      mg[counter2]->Add(&edata[counter]);
      } // end res1 
      
	  
      if(i==4){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi^{+} + n "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==5 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
      }
      
        if(i==6){
	counter2++;
	mg[counter2]->SetTitle("#nu+p #rightarrow #mu^{-} + #Delta^{+} decay in #pi^{0} + p "); // igual
//	mg[counter2]->Add(&edata[counter]);

      }
      
      if(i==7 && deu){
	  counter++;
	  continue;
//	mg[counter2]->Add(&edata[counter]);
	
  
      }
      
    cout << counter2 << " c2 " << endl;  
    mg[counter2]->Add(&edata[counter]);	  
    counter++;  
  }

  
  counter2++;
  
  TString Nbaseres  = "NCres%de";
  TString Nresname;

  TString name[4] = {"#nu+p #rightarrow #nu + #Delta^{+} decay in #pi^{+} + n ","#nu+p #rightarrow #nu + #Delta^{+} decay in #pi{0} + p ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{0} + n ","#nu+p #rightarrow #nu + #Delta^{0} decay in #pi^{-} + p "};
  
     
  Nresname= Form(Nbaseres.Data(), 1);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[0]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;

  
  Nresname= Form(Nbaseres.Data(), 2);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 3);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[1]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;

  Nresname= Form(Nbaseres.Data(), 4);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[2]); 
  mg[counter2]->Add(&edata[counter]);
  counter2++;
  counter++;
  
  
  Nresname= Form(Nbaseres.Data(), 5);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
  counter++;
  
  Nresname= Form(Nbaseres.Data(), 6);
  edata[counter] = TGraphErrors("data/"+Nresname+".txt","%lg %lg %lg");
  mg[counter2]->SetTitle(name[3]); 
  mg[counter2]->Add(&edata[counter]);
 // counter2++;
  
  
  cout << counter << endl;
  cout << counter2 << endl;

 // /home/jlbernal/cross/data/NCres1e.txt

  
      TString pbase  = "res/test%de.pdf";
      TString npr;
      for(int i = 0; i < 8; i++){
        npr= Form(pbase.Data(), i);
	cout << npr << endl;
       	TCanvas *c1 = new TCanvas("c1","",800,600) ;
	mg[i]->Draw("AP");
	c1->Print(npr);
	delete c1;
    } 
  
 
    TString pbase1  = "res/res%d.pdf";  
    TString outname = Form(pbase1.Data(), pos);;

    TCanvas *c1 = new TCanvas("c1","",800,600) ;
    mg[pos]->Draw("AP");
    mg[pos]->GetXaxis()->SetTitle("Energy (GeV)");
    mg[pos]->GetXaxis()->CenterTitle();
    mg[pos]->GetYaxis()->SetTitle("Cross Section (10^{-38} cm^{-2})");
    mg[pos]->GetYaxis()->CenterTitle();
    mg[pos]->GetYaxis()->SetTitleOffset(1.4); 
    mg[pos]->GetXaxis()->SetRangeUser(0.1,1.8);

    TLegend *leg = new TLegend(0.7, 0.74, 0.9, 0.87);
    leg->AddEntry(ptg, "True", "p");
    leg->AddEntry(ptgl, "Like", "p");
    leg->AddEntry(&edata[pos+1], "Experimental", "pe");
    leg->Draw();

    
   gPad->Modified();
    //c1->SetLogx();  
    c1->Print(outname);
    delete c1;
    delete leg;
    // _________________________________________________________
    
    if(pos==0)
      cout << "free pass " << endl;
    else{
    TMultiGraph *nmg = new TMultiGraph();
    
    
     TString anames[8]  = {" ", 
"#nu+n -> #Delta^{++} + #mu^{-} -> #mu^{-} + #pi^{+} + p ",
 "#nu+n -> #Delta^{+} + #mu^{-} -> #mu^{-} + #pi^{+} + n ",
 "#nu+n -> #Delta^{+} + #mu^{-} -> #mu^{-} + #pi^{0} + p ",
 "#nu+p -> #Delta^{+} + #nu -> #nu + #pi^{+} + n ",
 "#nu+p -> #Delta^{+} + #nu -> #nu + #pi^{0} + p ",
 "#nu+n -> #Delta^{0} + #nu -> #nu + #pi^{0} + n ",
 "#nu+n -> #Delta^{0} + #nu -> #nu + #pi^{-} + p "};
    
    nmg->SetTitle(anames[pos]);  
    TFile* f = NULL;
   
    TString a = "export/%d.root";
    TString mfname = Form(a.Data(), (int)pos);    
    f = new TFile(mfname);
    f->ls();  
    
    TGraph *mcross = (TGraph*)f->Get("Graph");
    
    TCanvas *c2 = new TCanvas("c1","",800,600) ;
    nmg->Add(mcross);
    nmg->Add(ptg);
    nmg->Add(ptgl);
    nmg->Draw("AP");
    nmg->GetXaxis()->SetTitle("Energy (GeV)");
    nmg->GetXaxis()->CenterTitle();
    nmg->GetYaxis()->SetTitle("Cross Section (10^{-38} cm^{-2})");
    nmg->GetYaxis()->CenterTitle();
    nmg->GetYaxis()->SetTitleOffset(1.4); 
    nmg->GetXaxis()->SetRangeUser(0.1,1.8);


    TLegend *leg = new TLegend(0.91, 0.8, 0.99, 0.9);
    leg->AddEntry(ptg, "True", "p");
    leg->AddEntry(ptgl, "Like", "p");
    leg->AddEntry(mcross, "Model", "p");
    leg->Draw();

    
   gPad->Modified();

    
    TString pbase2  = "res/airt%d.pdf";  
    TString outname2 = Form(pbase2.Data(), pos);;
    
    
    c2->Print(outname2)  ;
    
    delete c2;
    delete f;
    delete nmg;
    }
    
    
}

