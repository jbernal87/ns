#ifndef __DataHelper_pHH
#define __DataHelper_pHH

#include "TString.h"
#include "TFile.h"
#include "TNtuple.h"
#include <iostream>
#include <string>
#include <sstream>
#include <fstream>

const Int_t MAXNUC = 250;
const Int_t MAXLEV = 30;
const Int_t MAXPRC = 10;
const Int_t MAXUNBND = 500;

struct DataHelper_struct{
	TFile *rootFile;
	TNtuple *tuple;
	Float_t* columns;
	TString *varList;
	Int_t columnsLength;
	DataHelper_struct(const char* var_List, const char* tupleName = "default.root");
	virtual ~DataHelper_struct(){
		// std::cout << "dying ..." << std::endl;
		if(varList) delete varList;
		if(columns) delete columns;
		if(tuple) delete tuple;

		if(rootFile){ rootFile->Close(); delete rootFile; }
	}
	Int_t ReadDataFrom(const char* fileName);  
};
	struct Nuc_Struct{
		Int_t A, Z;
		Int_t PLevels[MAXLEV];
		Int_t NLevels[MAXLEV];
		Int_t Nuc_IDX[MAXNUC];
		Int_t Nuc_PID[MAXNUC];
		bool Nuc_IsB[MAXNUC];
		Double_t Nuc_X[MAXNUC];
		Double_t Nuc_Y[MAXNUC];
		Double_t Nuc_Z[MAXNUC];
		Double_t Nuc_P[MAXNUC];
		Double_t Nuc_PX[MAXNUC];
		Double_t Nuc_PY[MAXNUC];
		Double_t Nuc_PZ[MAXNUC];
		Double_t Nuc_E[MAXNUC];
		Double_t Nuc_K[MAXNUC];
		void Print();
		void Print_Levels();
		void CPT_Print(ofstream &out);
	} ;
	struct  Mes_Struct{
		Int_t NM;
		Int_t Mes_KEY[MAXNUC];
		Int_t Mes_PID[MAXNUC];
		bool Mes_IsB[MAXNUC];
		Double_t Mes_X[MAXNUC];
		Double_t Mes_Y[MAXNUC];
		Double_t Mes_Z[MAXNUC];
		Double_t Mes_P[MAXNUC];
		Double_t Mes_PX[MAXNUC];
		Double_t Mes_PY[MAXNUC];
		Double_t Mes_PZ[MAXNUC];
		Double_t Mes_E[MAXNUC];
		Double_t Mes_K[MAXNUC];
		void Print();
		void CPT_Print(ofstream &out);
	};
	struct Proc_Struct{
		Int_t ProN;
		Double_t TIME;
		Int_t I1,I2;
		Int_t NPartIn;
		Int_t NTotal;
		Int_t Proc_PID[MAXPRC];
		bool Proc_IsB[MAXPRC];
		bool Proc_IsF[MAXPRC]; // (Is Final) false = initial particles, true final particles
		Double_t Proc_X[MAXPRC];
		Double_t Proc_Y[MAXPRC];
		Double_t Proc_Z[MAXPRC];
		Double_t Proc_P[MAXPRC];
		Double_t Proc_PX[MAXPRC];
		Double_t Proc_PY[MAXPRC];
		Double_t Proc_PZ[MAXPRC];
		Double_t Proc_E[MAXPRC];
		Double_t Proc_K[MAXPRC];
		void Print();
		void CPT_Print(ofstream &out);
	};

	struct UnBindPart_Struct{
		Int_t NPart;
		Int_t Part_PID[MAXUNBND];
		Double_t Part_PX[MAXUNBND];
		Double_t Part_PY[MAXUNBND];
		Double_t Part_PZ[MAXUNBND];
		Double_t Part_E[MAXUNBND];
		Double_t Part_K[MAXUNBND];
		void Print();
		void CPT_Print(ofstream &out);
	};

	
	struct Fission{
	  Int_t fiss_A;
	  Int_t fiss_Z; 
	  Double_t fiss_E;//final configuration
	  Double_t fissility;
	  Int_t fiss_neutron;
	  Int_t fiss_proton;
	  Int_t fiss_alpha; //multiplicities
	};
	struct Spallation{
	  Int_t spall_A;
	  Int_t spall_Z; //final configuration
	  Int_t spall_neutron;
	  Int_t spall_proton;
	  Int_t spall_alpha; //multiplicities
	};
	
	
typedef DataHelper_struct DataHelper;

#endif
